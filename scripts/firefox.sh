#!/bin/bash
set -ex

apt-get install -y firefox
cp /usr/share/applications/firefox.desktop $HOME/Desktop/
chown 1000:1000 $HOME/Desktop/firefox.desktop
