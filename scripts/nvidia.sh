
#!/bin/bash
set -ex

apt-get update
apt-get install -y -f libnvidia-compute-525 nvidia-utils-525 nvidia-cg-toolkit nvidia-cuda-toolkit jupyter-notebook python-pandas python3-pandas python3 hashcat hashcat-nvidia python-all python-all-dev 