kasm-nvidia-dev
---

Kasm Nvidia Development Desktop Package


![Alt text](Screenshot.png "Kasm Nvidia Development Desktop")


Build
---

```
$ make build
```

Or root:
```
$ sudo make build
```





Pull package from docker hub;

```
docker pull oytunistrator/kasm-nvidia-dev:latest
```


After add `oytunistrator/kasm-nvidia-dev:latest` to your kasm panel. 

Dependens
---

- [Docker](https://www.docker.com/)
- [Kasm Workspaces](https://www.kasmweb.com/)


Package List:
---
- php 8.x
- composer 2.x
- laravel 8.x
- visual studio code
- sublime
- chromium
- firefox
- discord
- telegram
- filezilla
- signal
- remmina
- microsoft teams (beta)
- libre office
- vlc
- obs
- openshot
- postman
- geany
- gimp
- inkscape
- nvidia-cuda
- hashcat